variable "do_token" {}
variable "pub_key" {}
variable "pvt_key" {}
variable "ssh_fingerprint" {}
variable "gitlab_token" {}
variable "gitlab_runner_token" {}
variable "CF_API_EMAIL" {}
variable "CF_API_KEY" {}
variable "grafana_user" {}
variable "grafana_password" {}
variable "nextcloud_username" {}
variable "nextcloud_password" {}
variable "nextcloud_external_bucket_key" {}
variable "nextcloud_external_bucket_secret" {}
variable "nextcloud_mariadb_username" {}
variable "nextcloud_mariadb_password" {}
variable "mongo_root_password" {}
variable "mongo_username" {}
variable "mongo_password" {}
variable "elastic_user" {}
variable "elastic_password" {}
variable "golang_api_key" {}
variable "vault_bucket_key" {}
variable "vault_bucket_secret" {}
# variable "vault_root_token" {}

terraform {
  backend "s3" {
    bucket     = "terraform-k8s-cmj"
    key        = "do-k8s-cmj/terraform.tfstate"
    region     = "us-west-002"
    endpoint   = "https://s3.us-west-002.backblazeb2.com"

    skip_region_validation      = true
    skip_credentials_validation = true
    skip_metadata_api_check     = true
    force_path_style            = true
  }

  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "2.3.0"
    }

    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "2.18.0"
    }

    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "3.3.0"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "1.13.3"
    }

    kubernetes-alpha = {
      source = "hashicorp/kubernetes-alpha"
      version = "0.2.1"
    }

    helm = {
      source  = "hashicorp/helm"
      version = "2.0.2"
    }

    kubectl = {
      source = "gavinbunney/kubectl"
      version = "1.10.0"
    }
  }
}

provider "digitalocean" {
  token = var.do_token
}

provider "cloudflare" {
  email   = var.CF_API_EMAIL
  api_key = var.CF_API_KEY
}

provider "gitlab" {
  token = var.gitlab_token
}

provider "kubectl" {
  host  = digitalocean_kubernetes_cluster.k8s-cmj-cluster.endpoint
  token = digitalocean_kubernetes_cluster.k8s-cmj-cluster.kube_config[0].token
  cluster_ca_certificate = base64decode(
    digitalocean_kubernetes_cluster.k8s-cmj-cluster.kube_config[0].cluster_ca_certificate
  )
}

provider "kubernetes" {
  load_config_file = false
  host  = digitalocean_kubernetes_cluster.k8s-cmj-cluster.endpoint
  token = digitalocean_kubernetes_cluster.k8s-cmj-cluster.kube_config[0].token
  cluster_ca_certificate = base64decode(
    digitalocean_kubernetes_cluster.k8s-cmj-cluster.kube_config[0].cluster_ca_certificate
  )
}

provider "kubernetes-alpha" {
  host  = digitalocean_kubernetes_cluster.k8s-cmj-cluster.endpoint
  token = digitalocean_kubernetes_cluster.k8s-cmj-cluster.kube_config[0].token
  cluster_ca_certificate = base64decode(
    digitalocean_kubernetes_cluster.k8s-cmj-cluster.kube_config[0].cluster_ca_certificate
  )
}

provider "helm" {
  kubernetes {
    host  = digitalocean_kubernetes_cluster.k8s-cmj-cluster.endpoint
    token = digitalocean_kubernetes_cluster.k8s-cmj-cluster.kube_config[0].token
    cluster_ca_certificate = base64decode(
      digitalocean_kubernetes_cluster.k8s-cmj-cluster.kube_config[0].cluster_ca_certificate
    )
  }
}

