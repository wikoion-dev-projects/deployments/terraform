# variable "CF_API_EMAIL" {}
# variable "CF_API_KEY" {}

terraform {
  required_providers {
    helm = {
      source  = "hashicorp/helm"
      version = "2.0.2"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "1.13.3"
    }
  }
}

resource "kubernetes_namespace" "ns-telegraf" {
  metadata {
    name = "telegraf"
  }
}

resource "helm_release" "helm-telegraf-operator" {
  name  = "telegraf-operator"
  repository = "https://helm.influxdata.com/"
  chart = "telegraf-operator"
  namespace = "telegraf"

  values = [
    file("${path.module}/telegraf-operator-values.yaml")
  ]
}

resource "helm_release" "helm-telegraf-ds" {
  name  = "telegraf-ds"
  repository = "https://helm.influxdata.com/"
  chart = "telegraf-ds"
  namespace = "telegraf"

  values = [
    file("${path.module}/telegraf-ds-values.yaml")
  ]
}
