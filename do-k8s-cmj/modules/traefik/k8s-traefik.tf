variable "CF_API_EMAIL" {}
variable "CF_API_KEY" {}

terraform {
  required_providers {
    helm = {
      source  = "hashicorp/helm"
      version = "2.0.2"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "1.13.3"
    }
  }
}

resource "kubernetes_namespace" "traefik" {
  metadata {
    name = "traefik"
  }
}

resource "kubernetes_secret" "cloudflare-creds" {
  metadata {
    name = "cloudflare-creds"
    namespace = "traefik"
  }

  data = {
    CF_API_EMAIL = var.CF_API_EMAIL
    CF_API_KEY = var.CF_API_KEY
  }

  depends_on = [
    kubernetes_namespace.traefik
  ]
}

resource "helm_release" "traefik" {
  name  = "traefik"
  repository = "https://helm.traefik.io/traefik"
  chart = "traefik"
  namespace = "traefik"

  values = [
    file("${path.module}/traefik-values.yaml")
  ]

  depends_on = [
    kubernetes_secret.cloudflare-creds
  ]
}
