variable "mongo_root_password" {}
variable "mongo_username" {}
variable "mongo_password" {}
variable "elastic_user" {}
variable "elastic_password" {}
variable "golang_api_key" {}
variable "api_host" {}
variable "api_domain" {}
variable "blog_host" {}
variable "blog_domain" {}
variable "cf_zone_id" {}
variable "ip" {}

terraform {
  required_providers {
    kubectl = {
      source = "gavinbunney/kubectl"
      version = "1.10.0"
    }

    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "2.18.0"
    }
  }
}

resource "helm_release" "mongodb" {
  name  = "mongodb"
  repository = "https://charts.bitnami.com/bitnami"
  chart = "mongodb"
  namespace = "mongodb"
  create_namespace = true

  set {
    name = "architecture"
    value = "replicaset"
  }

  set {
    name = "auth.rootPassword"
    value = var.mongo_root_password
  }

  set {
    name = "auth.username"
    value = var.mongo_username
  }

  set {
    name = "auth.password"
    value = var.mongo_password
  }

  set {
    name = "auth.database"
    value = "coreDB"
  }
}

resource "kubernetes_namespace" "elasticsearch" {
  metadata {
    name = "elasticsearch"
  }
}

resource "kubernetes_secret" "elastic-creds" {
  metadata {
    name = "elastic-creds"
    namespace = "elasticsearch"
  }

  data = {
    username = var.elastic_user
    password = var.elastic_password
  }

  depends_on = [
    kubernetes_namespace.elasticsearch
  ]
}

resource "helm_release" "elasticsearch" {
  name  = "elasticsearch"
  repository = "https://helm.elastic.co"
  chart = "elasticsearch"
  namespace = "elasticsearch"

  values = [
    file("${path.module}/elasticsearch-values.yaml")
  ]

  depends_on = [
    kubernetes_secret.elastic-creds
  ]
}

resource "cloudflare_record" "api" {
  zone_id = var.cf_zone_id
  name    = "api"
  value   = var.ip
  type    = "A"
  proxied =  true
}

resource "kubernetes_namespace" "blog" {
  metadata {
    name = "blog"
  }
}

resource "kubernetes_secret" "golang-blog-api-key" {
  metadata {
    name = "golang-blog-api-key"
    namespace = "blog"
  }

  data = {
    go-api-key = var.golang_api_key
  }

  depends_on = [
    kubernetes_namespace.blog
  ]
}

resource "kubernetes_secret" "golang-blog-mongo-dsn" {
  metadata {
    name = "golang-blog-mongo-dsn"
    namespace = "blog"
  }

  data = {
    mongo-dsn = "mongodb://${var.mongo_username}:${var.mongo_password}@mongodb-headless.mongodb.svc.cluster.local/coreDB?replicaSet=rs0&ssl=false"
  }

  depends_on = [
    kubernetes_namespace.blog
  ]
}

resource "helm_release" "golang-blog" {
  name  = "golang-blog"
  chart = "../golang-blog"
  namespace = "blog"
  create_namespace = true

  depends_on = [
    helm_release.elasticsearch,
    helm_release.mongodb,
    kubernetes_namespace.blog
  ]
}

## Temporary work around until alpha provider is working correctly
resource "kubectl_manifest" "golang-blog-ingress" {
  yaml_body = <<YAML
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: golang-blog
  namespace: blog
spec:
  entryPoints:
    - websecure
  routes:
    - kind: Rule
      match: Host(`${var.api_host}.${var.api_domain}`)
      services:
        - name: golang-blog
          port: 8080
  tls:
    certResolver: le
YAML

  depends_on = [
    helm_release.golang-blog
  ]
}

resource "cloudflare_record" "blog" {
  zone_id = var.cf_zone_id
  name    = "blog"
  value   = var.ip
  type    = "A"
  proxied =  true
}

resource "helm_release" "reframe-blog" {
  name  = "reframe-blog"
  chart = "../reframe-blog"
  namespace = "blog"

  depends_on = [
    helm_release.elasticsearch,
    helm_release.mongodb,
    helm_release.golang-blog
  ]
}

## Temporary work around until alpha provider is working correctly
resource "kubectl_manifest" "reframe-blog-ingress" {
  yaml_body = <<YAML
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: reframe-blog
  namespace: blog
spec:
  entryPoints:
    - websecure
  routes:
    - kind: Rule
      match: Host(`${var.blog_host}.${var.blog_domain}`)
      services:
        - name: reframe-blog
          port: 3000
  tls:
    certResolver: le
YAML

  depends_on = [
    helm_release.reframe-blog
  ]
}
