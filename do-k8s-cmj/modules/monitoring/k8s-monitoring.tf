variable "grafana_user" {}
variable "grafana_password" {}
variable "cf_zone_id" {}
variable "ip" {}

terraform {
  required_providers {
    kubectl = {
      source = "gavinbunney/kubectl"
      version = "1.10.0"
    }

    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "2.18.0"
    }
  }
}

resource "cloudflare_record" "anal" {
  zone_id = var.cf_zone_id
  name    = "anal"
  value   = var.ip
  type    = "A"
  proxied =  true
}

resource "kubernetes_namespace" "prometheus" {
  metadata {
    name = "prometheus"
  }
}

resource "helm_release" "prometheus" {
  name  = "prometheus"
  repository = "https://prometheus-community.github.io/helm-charts"
  chart = "prometheus"
  namespace = "prometheus"

  values = [
    file("${path.module}/prometheus-values.yaml")
  ]

  depends_on = [
    kubernetes_namespace.prometheus
  ]
}

resource "kubernetes_secret" "grafana-creds" {
  metadata {
    name = "grafana-creds"
    namespace = "prometheus"
  }

  data = {
    admin-user = var.grafana_user
    admin-password = var.grafana_password
  }
  depends_on = [
    helm_release.prometheus
  ]
}

resource "helm_release" "grafana" {
  name  = "grafana"
  repository = "https://grafana.github.io/helm-charts"
  chart = "grafana"
  namespace = "prometheus"

  values = [
    file("${path.module}/grafana-values.yaml")
  ]

  depends_on = [
    helm_release.prometheus,
    kubernetes_secret.grafana-creds
  ]
}

## Temporary work around until alpha provider is working correctly
resource "kubectl_manifest" "prometheus-ingress" {
  yaml_body = <<YAML
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: grafana
  namespace: prometheus
spec:
  entryPoints:
    - websecure
  routes:
    - kind: Rule
      match: Host(`anal.fossg.news`)
      services:
        - name: grafana
          port: 80
  tls:
    certResolver: le
YAML

  depends_on = [
    helm_release.prometheus,
    kubernetes_secret.grafana-creds,
    helm_release.grafana
  ]
}

## This is blocked until local plan is implemented in the kubernetes-alpha provider
# resource "kubernetes_manifest" "prometheus-ingress" {
#   provider = kubernetes-alpha
#   manifest = {
#     "apiVersion"    = "traefik.containo.us/v1alpha1"
#     "kind"          = "IngressRoute"
#     "metadata"      = {
#       "name" = "grafana"
#       "namespace" = "prometheus"
#     }
#     "spec"          = {
#       "entryPoints" = [
#         "websecure"
#       ]
#       "routes" = [
#         {
#           "match"     = "Host(`anal.fossg.news`)"
#           "kind"      = "Rule"
#           "services"  = [
#             {
#               "name" = "grafana"
#               "port" = "80"
#             }
#           ]
#         }
#       ]
#       "tls" = {
#         "certResolver" = "le"
#       }
#     }
#   }
#   depends_on = [
#     helm_release.grafana
#   ]
# }
