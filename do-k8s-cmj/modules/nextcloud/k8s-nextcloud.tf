variable "nextcloud_host" {}
variable "nextcloud_username" {}
variable "nextcloud_password" {}
variable "nextcloud_external_bucket_key" {}
variable "nextcloud_external_bucket_secret" {}
variable "nextcloud_mariadb_username" {}
variable "nextcloud_mariadb_password" {}
variable "cf_zone_id" {}
variable "ip" {}

terraform {
  required_providers {
    kubectl = {
      source = "gavinbunney/kubectl"
      version = "1.10.0"
    }

    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "2.18.0"
    }
  }
}

resource "cloudflare_record" "files" {
  zone_id = var.cf_zone_id
  name    = "files"
  value   = var.ip
  type    = "A"
  proxied =  true
}

resource "kubernetes_namespace" "nextcloud" {
  metadata {
    name = "nextcloud"
  }
}

resource "kubernetes_secret" "nextcloud" {
  metadata {
    name = "nextcloud"
    namespace = "nextcloud"
  }

  data = {
    nextcloud-username = var.nextcloud_username
    nextcloud-password = var.nextcloud_password
  }

  depends_on = [
    kubernetes_namespace.nextcloud
  ]
}

resource "kubernetes_secret" "nextcloud-external-bucket" {
  metadata {
    name = "nextcloud-external-bucket"
    namespace = "nextcloud"
  }

  data = {
    bucket-id = var.nextcloud_external_bucket_key
    bucket-secret = var.nextcloud_external_bucket_secret
  }

  depends_on = [
    kubernetes_namespace.nextcloud
  ]
}

resource "helm_release" "nextcloud" {
  name  = "nextcloud"
  repository = "https://nextcloud.github.io/helm"
  chart = "nextcloud"
  namespace = "nextcloud"

  values = [
    file("${path.module}/nextcloud-values-s3.yaml")
  ]

  set {
    name = "nextcloud.host"
    value = var.nextcloud_host
  }

  set {
    name = "nextcloud.existingSecret.enabled"
    value = true
  }

  set {
    name = "nextcloud.existingSecret.secretName"
    value = kubernetes_secret.nextcloud.metadata[0].name
  }
  
  set {
    name = "nextcloud.existingSecret.usernameKey"
    value = "nextcloud-username"
  }
  
  set {
    name = "nextcloud.existingSecret.passwordKey"
    value = "nextcloud-password"
  }

  set {
    name = "mariadb.db.name"
    value = "nextcloud"
  }

  set {
    name = "mariadb.db.user"
    value = var.nextcloud_mariadb_username
  }

  set {
    name = "mariadb.db.password"
    value = var.nextcloud_mariadb_password
  }

  depends_on = [
    kubernetes_namespace.nextcloud,
    kubernetes_secret.nextcloud,
    kubernetes_secret.nextcloud-external-bucket
  ]
}

## Temporary work around until alpha provider is working correctly
resource "kubectl_manifest" "nextcloud-ingress" {
  yaml_body = <<YAML
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: nextcloud
  namespace: nextcloud
spec:
  entryPoints:
    - websecure
  routes:
    - kind: Rule
      match: Host(`${var.nextcloud_host}`)
      services:
        - name: nextcloud
          port: 8080
  tls:
    certResolver: le
YAML

  depends_on = [
    kubernetes_namespace.nextcloud
  ]
}

## Blocked by terraform local plan impl
# resource "kubernetes_manifest" "nextcloud-ingress" {
#   provider = kubernetes-alpha
#   manifest = {
#     "apiVersion"    = "traefik.containo.us/v1alpha1"
#     "kind"          = "IngressRoute"
#     "metadata"      = {
#       "name" = "nextcloud"
#       "namespace" = "nextcloud"
#     }
#     "spec"          = {
#       "entryPoints" = [
#         "websecure"
#       ]
#       "routes" = [
#         {
#           "match"     = "Host(`files.fossg.news`)"
#           "kind"      = "Rule"
#           "services"  = [
#             {
#               "name" = "nextcloud"
#               "port" = "8099"
#             }
#           ]
#         }
#       ]
#       "tls" = {
#         "certResolver" = "le"
#       }
#     }
#   }
#   depends_on = [
#     helm_release.nextcloud
#   ]
# }
