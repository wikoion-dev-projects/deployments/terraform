variable "vault_bucket_key" {}
variable "vault_bucket_secret" {}
variable "vault_host" {}
variable "vault_domain" {}
variable "cf_zone_id" {}
variable "ip" {}

terraform {
  required_providers {
    kubectl = {
      source = "gavinbunney/kubectl"
      version = "1.10.0"
    }

    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "2.18.0"
    }
  }
}

resource "cloudflare_record" "vault" {
  zone_id = var.cf_zone_id
  name    = var.vault_host
  value   = var.ip
  type    = "A"
  proxied =  true
}

resource "helm_release" "vault" {
  name  = "vault"
  repository = "https://helm.releases.hashicorp.com"
  chart = "vault"
  namespace = "vault"
  create_namespace = true

  values = [
    file("${path.module}/vault-values.yaml")
  ]

  set {
    name = "server.standalone.config"
    value = <<EOF
  ui = true
  listener "tcp" {
    tls_disable = 1
    address = "[::]:8200"
    cluster_address = "[::]:8201"
  }
  storage "s3" {
    bucket     = "vault-k8s-cmj"
    region     = "us-west-002"
    endpoint   = "https://s3.us-west-002.backblazeb2.com"
    access_key = "${var.vault_bucket_key}"
    secret_key = "${var.vault_bucket_secret}"
    s3_force_path_style = "true"
  }
EOF
  }
}

## Temporary work around until alpha provider is working correctly
resource "kubectl_manifest" "vault-ingress" {
  yaml_body = <<YAML
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: vault
  namespace: vault
spec:
  entryPoints:
    - websecure
  routes:
    - kind: Rule
      match: Host(`${var.vault_host}.${var.vault_domain}`)
      services:
        - name: vault
          port: 8200
  tls:
    certResolver: le
YAML

  depends_on = [
    helm_release.vault
  ]
}
