variable "gitlab_runner_token" {}
variable "kubernetes_api_url" {}
variable "kubernetes_token" {}
variable "kubernetes_ca_cert" {}

terraform {
  required_providers {
    helm = {
      source  = "hashicorp/helm"
      version = "2.0.2"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "1.13.3"
    }

    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "3.3.0"
    }
  }
}

resource "gitlab_group_cluster" "k8s-cmj-cluster" {
  group                         = "5243695"
  name                          = "k8s-cmj-cluster"
  enabled                       = true
  kubernetes_api_url            = var.kubernetes_api_url
  kubernetes_token              = var.kubernetes_token
  kubernetes_ca_cert            = var.kubernetes_ca_cert
  kubernetes_authorization_type = "rbac"
}

resource "kubernetes_cluster_role" "secrets" {
  metadata {
    name = "secrets"
  }

  rule {
    api_groups = ["extensions", "apps", "traefik.containo.us"]
    resources  = ["deployments", "ingressroutes", "namespaces", "statefulsets"]
    verbs      = ["get", "list", "watch", "create", "patch", "delete"]
  }
}

resource "kubernetes_cluster_role_binding" "gitlab-admin" {
  metadata {
    name = "gitlab-admin"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "secrets"
  }

  subject {
    kind      = "ServiceAccount"
    name      = "gitlab-runner-gitlab-runner"
    namespace = "gitlab"
  }
}

resource "helm_release" "gitlab-runner" {
  name  = "gitlab-runner"
  repository = "https://charts.gitlab.io"
  chart = "gitlab-runner"
  namespace = "gitlab"
  create_namespace = true

  values = [
    file("${path.module}/gitlab-values.yaml")
  ]

  set {
    name = "runnerRegistrationToken"
    value = var.gitlab_runner_token
  }
}
