resource "digitalocean_kubernetes_cluster" "k8s-cmj-cluster" {
  name    = "k8s-cmj-cluster"
  region  = "lon1"
  # Grab the latest version slug from `doctl kubernetes options versions`
  version = "1.24.4-do.0"

  node_pool {
    name       = "k8s-cmj-pool"
    size       = "s-1vcpu-2gb"
    node_count = 2
    labels = {
      memory = "low"
    }
  }
}

resource "digitalocean_kubernetes_node_pool" "k8s-cmj-pool-high-mem" {
  cluster_id = digitalocean_kubernetes_cluster.k8s-cmj-cluster.id
  name       = "k8s-cmj-pool-high-mem"
  size       = "s-2vcpu-4gb"
  node_count = 1
  labels = {
    memory = "high"
  }
}

data "cloudflare_zones" "fossg" {
  filter {
    name = "fossg.news"
  }
}

module "traefik" {
  source = "./modules/traefik"

  CF_API_EMAIL = var.CF_API_EMAIL
  CF_API_KEY = var.CF_API_KEY
  
  depends_on = [
    digitalocean_kubernetes_cluster.k8s-cmj-cluster
  ]
}

data "digitalocean_loadbalancer" "traefik-fossg" {
  name = "traefik-fossg"

  depends_on = [
    digitalocean_kubernetes_cluster.k8s-cmj-cluster,
    module.traefik
  ]
}

module "vault" {
  source = "./modules/vault"

  vault_bucket_key = var.vault_bucket_key
  vault_bucket_secret = var.vault_bucket_secret
  vault_host = "nox"
  vault_domain = "fossg.news"
  cf_zone_id = lookup(data.cloudflare_zones.fossg.zones[0], "id")
  ip = data.digitalocean_loadbalancer.traefik-fossg.ip
  
  depends_on = [
    digitalocean_kubernetes_cluster.k8s-cmj-cluster,
    module.traefik
  ]
}

module "gitlab" {
  source = "./modules/gitlab"

  gitlab_runner_token = var.gitlab_runner_token
  kubernetes_api_url  = digitalocean_kubernetes_cluster.k8s-cmj-cluster.endpoint
  kubernetes_token    = digitalocean_kubernetes_cluster.k8s-cmj-cluster.kube_config[0].token
  kubernetes_ca_cert  = base64decode(
    digitalocean_kubernetes_cluster.k8s-cmj-cluster.kube_config[0].cluster_ca_certificate)
  
  depends_on = [
    digitalocean_kubernetes_cluster.k8s-cmj-cluster
  ]
}

# module "monitoring" {
#   source = "./modules/monitoring"

#   grafana_user = var.grafana_user
#   grafana_password = var.grafana_password
#   cf_zone_id = lookup(data.cloudflare_zones.fossg.zones[0], "id")
#   ip = data.digitalocean_loadbalancer.traefik-fossg.ip

#   depends_on = [
#     digitalocean_kubernetes_cluster.k8s-cmj-cluster,
#     module.traefik
#   ]
# }

module "nextcloud" {
  source = "./modules/nextcloud"

  nextcloud_host = "files.fossg.news"
  nextcloud_username = var.nextcloud_username
  nextcloud_password = var.nextcloud_password
  nextcloud_external_bucket_key = var.nextcloud_external_bucket_key
  nextcloud_external_bucket_secret = var.nextcloud_external_bucket_secret
  nextcloud_mariadb_username = var.nextcloud_mariadb_username
  nextcloud_mariadb_password = var.nextcloud_mariadb_password
  cf_zone_id = lookup(data.cloudflare_zones.fossg.zones[0], "id")
  ip = data.digitalocean_loadbalancer.traefik-fossg.ip

  depends_on = [
    digitalocean_kubernetes_cluster.k8s-cmj-cluster,
    module.traefik
  ]
}

module "telegraf-operator" {
  source = "./modules/telegraf"

  depends_on = [
    digitalocean_kubernetes_cluster.k8s-cmj-cluster,
    module.traefik
  ]
}

# module "blog" {
#   source = "./modules/go-reframe-blog"

#   mongo_root_password = var.mongo_root_password
#   mongo_username = var.mongo_username
#   mongo_password = var.mongo_password
#   elastic_user = var.elastic_user
#   elastic_password = var.elastic_password
#   golang_api_key = var.golang_api_key
#   blog_host = "blog"
#   blog_domain = "fossg.news"
#   api_host = "api"
#   api_domain = "fossg.news"
#   cf_zone_id = lookup(data.cloudflare_zones.fossg.zones[0], "id")
#   ip = data.digitalocean_loadbalancer.traefik-fossg.ip

#   depends_on = [
#     digitalocean_kubernetes_cluster.k8s-cmj-cluster,
#     module.traefik
#   ]
# }
