# Terraform deployment to kubernetes with traefik for ingress controller and Let's Encrypt certs
Setup vars
```
export DO_PAT="YOUR_DO_PERSONAL_ACCESS_TOKEN"
ssh-keygen -E md5 -lf ~/.ssh/id_rsa.pub | awk '{print $2}'
export DO_SSH_FINGERPRINT="e7:42:16:d7:e5:a0:43:29:82:7d:a0:59:cf:9e:92:f7" #<-- the output from the ssh-keygen command above
export GITLAB_TOKEN="YOUR_GITLAB_PERSONAL_ACCESS_TOKEN"
```
To build
```
terraform init

terraform plan \
  -var "do_token=${DO_PAT}" \
  -var "pub_key=$HOME/.ssh/id_rsa.pub" \
  -var "pvt_key=$HOME/.ssh/id_rsa" \
  -var "ssh_fingerprint=${DO_SSH_FINGERPRINT}" \
  -var "gitlab_token=${GITLAB_TOKEN}"

terraform apply \
  -var "do_token=${DO_PAT}" \
  -var "pub_key=$HOME/.ssh/id_rsa.pub" \
  -var "pvt_key=$HOME/.ssh/id_rsa" \
  -var "ssh_fingerprint=${DO_SSH_FINGERPRINT}" \
  -var "gitlab_token=${GITLAB_TOKEN}"
```
To Destroy
```
terraform plan -destroy -out=terraform.tfplan \
  -var "do_token=${DO_PAT}" \
  -var "pub_key=$HOME/.ssh/id_rsa.pub" \
  -var "pvt_key=$HOME/.ssh/id_rsa" \
  -var "ssh_fingerprint=${DO_SSH_FINGERPRINT}" \
  -var "gitlab_token=${GITLAB_TOKEN}"

terraform apply terraform.tfplan
```
If Terraform is out of sync

```
terraform refresh \
  -var "do_token=${DO_PAT}" \
  -var "pub_key=$HOME/.ssh/id_rsa.pub" \
  -var "pvt_key=$HOME/.ssh/id_rsa" \
  -var "ssh_fingerprint=${DO_SSH_FINGERPRINT}" \
  -var "gitlab_token=${GITLAB_TOKEN}"
```
