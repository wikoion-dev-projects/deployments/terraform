variable "do_token" {}
variable "pub_key" {}
variable "pvt_key" {}
variable "CF_API_EMAIL" {}
variable "CF_API_KEY" {}
variable "wg_admin_user" {}
variable "wg_admin_password" {}
variable "wg_wireguard_private_key" {}

terraform {
  backend "s3" {
    bucket     = "terraform-k8s-cmj"
    key        = "wireguard/terraform.tfstate"
    region     = "us-west-002"
    endpoint   = "https://s3.us-west-002.backblazeb2.com"

    skip_region_validation      = true
    skip_credentials_validation = true
    skip_metadata_api_check     = true
    force_path_style            = true
  }

  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "2.3.0"
    }

    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "2.18.0"
    }
  }
}

provider "digitalocean" {
  token = var.do_token
}

provider "cloudflare" {
  email   = var.CF_API_EMAIL
  api_key = var.CF_API_KEY
}
