resource "digitalocean_ssh_key" "wireguard" {
  name       = "wireguard"
  public_key = file(var.pub_key)
}

resource "digitalocean_droplet" "wireguard" {
  image  = "ubuntu-20-04-x64"
  name   = "wireguard1"
  region = "lon1"
  size   = "s-1vcpu-1gb"
   ssh_keys = [
     digitalocean_ssh_key.wireguard.id
  ]
  connection {
    host = self.ipv4_address
    user = "root"
    type = "ssh"
    private_key = file(var.pvt_key)
    timeout = "2m"
  }

  provisioner "file" {
    source      = "configure-wg.yml"
    destination = "$HOME/configure-wg.yml"
  }

  provisioner "file" {
    source      = "docker-compose.yml"
    destination = "$HOME/docker-compose.yml"
  }

  provisioner "file" {
    content = var.CF_API_EMAIL
    destination = "$HOME/CF_API_EMAIL.secret"
  }

  provisioner "file" {
    content = var.CF_API_KEY
    destination = "$HOME/CF_API_KEY.secret"
  }

  provisioner "remote-exec" {
    inline = [
      "export PATH=$PATH:/usr/bin",
      "sudo apt update -y",
      "sudo unattended-upgrade -d",
      "sudo apt -y install wireguard",
      "sudo apt -y install docker",
      "sudo apt -y install docker-compose",
      "sudo apt -y install ansible",
      "ansible-galaxy collection install community.docker",
      "sudo systemctl enable docker",
      "sudo systemctl start docker",
      "ansible-playbook -e 'swarm_ip=${digitalocean_droplet.wireguard.ipv4_address} CF_API_EMAIL=${var.CF_API_EMAIL} CF_API_KEY=${var.CF_API_KEY} wg_admin_user=${var.wg_admin_user} wg_admin_password=${var.wg_admin_password} wg_wireguard_private_key=${var.wg_wireguard_private_key}' --connection=local --inventory 127.0.0.1, $HOME/configure-wg.yml"
    ]
  }

  depends_on = [
     digitalocean_ssh_key.wireguard
  ]
}

resource "digitalocean_firewall" "wireguard" {
  name = "wireguard"

  droplet_ids = [digitalocean_droplet.wireguard.id]

  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = "80"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = "443"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = "8000"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol         = "udp"
    port_range       = "51820"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol         = "icmp"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol         = "udp"
    port_range       = "1-65535"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol         = "tcp"
    port_range       = "1-65535"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }
}

data "cloudflare_zones" "fossg" {
  filter {
    name = "fossg.news"
  }
}

resource "cloudflare_record" "wg" {
  zone_id = lookup(data.cloudflare_zones.fossg.zones[0], "id")
  name    = "wg"
  value   = digitalocean_droplet.wireguard.ipv4_address
  type    = "A"
}

